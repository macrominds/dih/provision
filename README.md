# dih provision Uberspace

Ansible playbook for dih Uberspace server provisioning.

## Prerequisites

Provide an ssh configuration named `dih` in your `.ssh/config`. Setup your
U7 Uberspace for this ssh connection by adding your public key as ssh-key 
in the dashboard.

Create a deployment key here: `~/.ssh/dih-deploy-key`
([Instructions](https://gitlab.com/macrominds/provision/uberspace/deployment-key#requirements)).

Run `make reqs` to install the requirements or `make force-reqs` to upgrade the requirements.

## Preparation run

The first deployment is somewhat special. So you might want to prepare everything but skip
setting the docroot, so that any old versions are still shown as long as we have nothing
deployed.

In that case, run `make dih-all-but-docroot`. It will perform every provision, but it won't 
(re)set the html link.

## Production run

```shell script
make dih
```

## Test run

Requires a U7 Uberspace and an `.ssh/config` setup for dih-test 

```shell script
make dih-test
```
