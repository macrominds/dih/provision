dih-all-but-docroot:
	ansible-playbook -i dih, site.yml --skip-tags=set_docroot

dih:
	ansible-playbook -i dih, site.yml

dih-test:
	ansible-playbook -i dih-test, site.yml

reqs:
	ansible-galaxy install -r requirements.yml

force-reqs:
	ansible-galaxy install -r requirements.yml  --force
